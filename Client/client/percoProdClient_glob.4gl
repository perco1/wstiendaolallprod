GLOBALS 

TYPE tadd_out RECORD           --Registro para almacenar la respuesta
         --CS ldescription CHAR(15),
         --CS lunit_price DECIMAL(10,2),
         det DYNAMIC ARRAY OF RECORD   --Detalle dentro del registro de respuesta
            barra       CHAR(13),
            casa        CHAR(2),
            correl      INTEGER,
            linea       CHAR(3),
            tamano      CHAR(10),
            medida      CHAR(10),
            precio      DECIMAL(10,2),
            existd      DECIMAL(10,2)  
         END RECORD 
    END RECORD

DEFINE add_out tadd_out 
    
END GLOBALS 