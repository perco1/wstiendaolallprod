#+
#+ Generated from calculatorClient
#+
IMPORT com
IMPORT xml
IMPORT util
IMPORT os

GLOBALS "percoProdClient_glob.4gl"

#+
#+ Global Endpoint user-defined type definition
#+
TYPE tGlobalEndpointType RECORD # Rest Endpoint
    Address RECORD # Address
        Uri STRING # URI
    END RECORD,
    Binding RECORD # Binding
        Version STRING, # HTTP Version (1.0 or 1.1)
        ConnectionTimeout INTEGER, # Connection timeout
        ReadWriteTimeout INTEGER, # Read write timeout
        CompressRequest STRING # Compression (gzip or deflate)
    END RECORD
END RECORD


   --Path del WS ejecutando el server en GeneroStudio FGLAPPSERVER
   --= (Address:(Uri: "http://192.168.43.129:8092/ws/r/productos"))  
   --Path del WS ejecutando el server en httpdispatch
   --= (Address:(Uri: "http://192.168.43.129:6494/ws/r/percoProdServer/productos)) 
   --Path del WS usando GAS
   --= (Address:(Uri: "http://192.168.43.129/gas/ws/r/percoProdServer/productos"))
    
PUBLIC DEFINE Endpoint
    tGlobalEndpointType
    --= (Address:(Uri: "http://192.168.43.129:6494/ws/r/percoProdServer/productos")) 
    = (Address:(Uri: "https://www.perfumeriafetiche.net/gas/ws/r/percoProdServer/productos")) 

# Error codes
PUBLIC CONSTANT C_SUCCESS = 0

################################################################################
# Operation /Modulo: Recibe como parámetro stock_num y manu_code, devuelve description y unit_price 
#                    y el detalle de las ordenes donde aparece este item (tabla items)  
#
# VERB: GET
#

--En calculatorClient_main se llama a esta función enviandole 2 parámetros y retornando 2 valores
--CS PUBLIC FUNCTION existencia(p_x INTEGER, p_y CHAR(3)) RETURNS(INTEGER, CHAR(15),DECIMAL(10,2) )  
--CS PUBLIC FUNCTION existencia() RETURNS(INTEGER, CHAR(15),DECIMAL(10,2) )  
PUBLIC FUNCTION existencia(uri STRING, userid STRING ) RETURNS(INTEGER)  
    DEFINE fullpath base.StringBuffer
    DEFINE query base.StringBuffer
    DEFINE contentType STRING
    DEFINE req com.HTTPRequest
    DEFINE resp com.HTTPResponse
    DEFINE resp_body STRING 

    TRY

        LET Endpoint.Address.Uri = uri

        # Prepare request path
        LET fullpath = base.StringBuffer.Create()
        LET query = base.StringBuffer.Create()
        CALL fullpath.append("/existencia") --Llama al método

        --Validación de parámetros para armar la petición (la variable query)
        IF userid IS NOT NULL THEN                    
            IF query.getLength() > 0 THEN
                CALL query.append(SFMT("&userid=%1", userid))
            ELSE
                CALL query.append(SFMT("userid=%1", userid))
            END IF
        END IF
        {CS IF p_y IS NOT NULL THEN
            IF query.getLength() > 0 THEN
                CALL query.append(SFMT("&y=%1", p_y))
            ELSE
                CALL query.append(SFMT("y=%1", p_y))
            END IF
        END IF }
        IF query.getLength() > 0 THEN
            CALL fullpath.append("?")
            CALL fullpath.append(query.toString())
        END IF  

        # Create request and configure it
        DISPLAY "Path ",  SFMT("%1%2", Endpoint.Address.Uri, fullpath.toString())
        LET req =
            com.HTTPRequest.Create(
                SFMT("%1%2", Endpoint.Address.Uri, fullpath.toString()))
        IF Endpoint.Binding.Version IS NOT NULL THEN
            CALL req.setVersion(Endpoint.Binding.Version)
        END IF
        IF Endpoint.Binding.ConnectionTimeout <> 0 THEN
            CALL req.setConnectionTimeout(Endpoint.Binding.ConnectionTimeout)
        END IF
        IF Endpoint.Binding.ReadWriteTimeout <> 0 THEN
            CALL req.setTimeout(Endpoint.Binding.ReadWriteTimeout)
        END IF
        IF Endpoint.Binding.CompressRequest IS NOT NULL THEN
            CALL req.setHeader(
                "Content-Encoding", Endpoint.Binding.CompressRequest)
        END IF

        # Perform request
        CALL req.setMethod("GET")
        CALL req.setHeader("Accept", "text/plain")
        CALL req.DoRequest()

        # Retrieve response
        LET resp = req.getResponse()
        # Process response
        INITIALIZE resp_body TO NULL
        LET contentType = resp.getHeader("Content-Type")
        
        CASE resp.getStatusCode()

            WHEN 200 #Success
                IF contentType MATCHES "*text/plain*" THEN
                    # Parse TEXT response
                    LET resp_body = resp.getTextResponse()
                    --DISPLAY "resp_body trae => ", resp_body
                    CALL util.JSON.parse(resp_body, add_out)
                    
                    --DISPLAY add_out.det[1].casa, add_out.det[1].correl 
                    RETURN C_SUCCESS --, add_out.ldescription, add_out.lunit_price
                END IF
                --CS RETURN -1, resp_body, NULL 
                RETURN -1

            OTHERWISE
                --CS RETURN resp.getStatusCode(), NULL, NULL
               RETURN resp.getStatusCode() 
        END CASE
    CATCH
        --CS RETURN -1, NULL, NULL 
        RETURN -1
    END TRY
END FUNCTION
################################################################################