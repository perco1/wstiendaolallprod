#
#       (c) Copyright 2008, ANDE, S.A. - www.solc.com.gt
#
#       Devolverá todos los productos, por
#       cada producto las columnas: Casa,
#       Correlativo, SKU, Precio y existencia
#       real.
#
#       calculatorClient.4gl Client of REST based web service example
#
#       Carlos Santizo Febrero 2024 
#

#+ calculatorClient.4gl
#+
#+ Client of REST based calculator web service example


IMPORT FGL percoProdClient    --Modulo 4GL que ejecuta la llamada al WS

GLOBALS "percoProdClient_glob.4gl"

PUBLIC DEFINE parameters RECORD
         cdbformat      STRING,
         cdbprot        STRING,
         cdbhost        STRING,
         cdbport        INTEGER,
         userid         STRING,
         cdbgas         CHAR(1),
         cdbconnector   STRING,
         cdbgroup       STRING,
         fmetodo        STRING,  
         --user_auth      STRING, -- Encrypted
         furl           STRING, 
         fcomando       STRING,
         fquery         STRING  
       END RECORD 

PUBLIC DEFINE cdburl STRING  

DEFINE win ui.Window,
   fm ui.FORM

MAIN
  --CS DEFINE stock_num   INTEGER         --Parámetro 1
  --CS DEFINE manu_code   CHAR(15)        --Parámetro 2
  --CS DEFINE description CHAR(15)       --Valor que retorna 1
  --CS DEFINE unit_price  DECIMAL(10,2)  --Valor que retorna 2

  DEFINE wsstatus INTEGER           --Resultado de la llamada al WS

  OPTIONS INPUT WRAP 
  
  CLOSE WINDOW SCREEN
  
  OPEN WINDOW w1 WITH FORM "percoProdClient" ATTRIBUTE (TEXT="Web Services Perco Productos") --,STYLE="naked")

  CALL load_settings()

  INPUT BY NAME parameters.cdbformat,
                parameters.cdbprot,
                parameters.cdbhost,
                parameters.cdbport,
                parameters.cdbgas,
                parameters.userid,
                parameters.cdbconnector,
                parameters.cdbgroup,
                parameters.fmetodo,
                parameters.fcomando,
                parameters.fquery
          WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED)
          
        BEFORE INPUT
           CALL DIALOG.setFieldActive("cdbformat",parameters.cdbformat=="json")
           CALL DIALOG.setFieldActive("cdbgroup",parameters.cdbgas=="Y")
           LET win = ui.Window.getCurrent()
           LET fm = win.getForm()
           --CALL fm.setElementHidden("l_snc",1)
           --CALL fm.setElementHidden("set_password",1)
           --CALL fm.setElementHidden("label_cnt",1)
           --CALL fm.setElementHidden("label_grp",1)
           --CALL fm.setFieldHidden("auto_sync",1)
           --CALL fm.setFieldHidden("autosync",1)
           --CALL fm.setFieldHidden("cdbgroup",1)
           --CALL fm.setFieldHidden("cdbconnector",1)
           --CALL fm.setFieldHidden("label_cnt",1)
           --CALL fm.setFieldHidden("sync_geoloc",1)
           --CALL fm.setFieldHidden("cdbgas",1)
           LET parameters.cdbgas = TRUE
           --DISPLAY params_cdb_url() TO furl
           LET parameters.furl = params_cdb_url()
           DISPLAY params_cdb_urltxt() TO furl 
           
        ON CHANGE cdbformat, cdbprot, cdbhost, cdbport,
                  cdbgas, cdbconnector, cdbgroup
           LET parameters.furl = params_cdb_url()
           DISPLAY params_cdb_urltxt() TO furl 
           --CALL DIALOG.setFieldActive("cdbconnector",parameters.cdbgas=="Y")
           --CALL DIALOG.setFieldActive("cdbgroup",parameters.cdbgas=="Y")

        ON ACTION consulta  
            CALL GET_FLDBUF(cdbprot, cdbhost, cdbport, cdbgas) 
               RETURNING parameters.cdbprot, parameters.cdbhost, parameters.cdbport, parameters.cdbgas
            LET parameters.furl = params_cdb_url()
            DISPLAY params_cdb_urltxt() TO furl 
            # Llamada al WS 
            --CS CALL exiClient.existencia(stock_num, manu_code) RETURNING wsstatus, description, unit_price
            --CS CALL exiClient.existencia() RETURNING wsstatus, description, unit_price
            CALL percoProdClient.existencia(parameters.furl, parameters.userid) RETURNING wsstatus 
            --DISPLAY "wsstatus ", wsstatus
            IF wsstatus = 0 THEN
               DISPLAY ARRAY add_out.det TO sdet.*
                  BEFORE DISPLAY
                     MESSAGE SFMT("Registros procesados: %1", add_out.det.getLength())
                     --EXIT DISPLAY

                  ON ACTION regresar
                     EXIT DISPLAY 
               END DISPLAY 
               CALL add_out.det.clear()
               DISPLAY ARRAY add_out.det TO sdet.*
                  BEFORE DISPLAY 
                     EXIT DISPLAY 
               END DISPLAY 
               MESSAGE "Total de registros procesados ", add_out.det.getLength()
            ELSE 
               ERROR "Error en los parámetros o Servidor fuera de línea, error: ", wsstatus
            END IF 
            --DISPLAY BY NAME description, unit_price    --Despliega resultado
        
     ON ACTION close
        EXIT INPUT
        
    END INPUT

  CLOSE WINDOW w1
  
END MAIN

PUBLIC FUNCTION params_cdb_url()
    DEFINE url STRING

    --Levantando el servidor manualmente
    IF parameters.cdbgas=="Y" THEN
       LET url = SFMT("%1://%2/gas",
                    parameters.cdbprot,
                    parameters.cdbhost)
    ELSE 
       LET url = SFMT("%1://%2:%3",
                    parameters.cdbprot,
                    parameters.cdbhost,
                    parameters.cdbport)
    END IF 

    LET url = url, SFMT("/ws/r/percoProdServer/%1",
                     parameters.cdbgroup)

{
LET url = url, SFMT("/ws/r/%1/%2?userid=%3",
                     parameters.cdbgroup,
                     parameters.fcomando,
                     parameters.userid)    
    IF parameters.cdbport = 80 THEN
       

    ELSE 
    
    END IF 
    LET url = SFMT("%1://%2:%3",
                   parameters.cdbprot,
                   parameters.cdbhost,
                   parameters.cdbport)

    IF parameters.cdbgas=="Y" THEN
    
       LET url = url, SFMT("%1/ws/r%2/dbsync_perco_server",
                           "/"||parameters.cdbconnector,
                           "/"||parameters.cdbgroup)
    END IF
    --LET url = url, "/ws/r/dbsync_perco_server/mobile/dbsync/", parameters.cdbformat
    LET url = url, "/ws/r/dbsync_perco_server/mobile/dbsync/", parameters.cdbformat}
    --DISPLAY "URL lleva -> ", url
    RETURN url
END FUNCTION

PUBLIC FUNCTION params_cdb_urltxt()
    DEFINE url STRING

    IF parameters.cdbgas=="Y" THEN
       LET url = SFMT("%1://%2/gas",
                    parameters.cdbprot,
                    parameters.cdbhost)
    ELSE 
       LET url = SFMT("%1://%2:%3",
                    parameters.cdbprot,
                    parameters.cdbhost,
                    parameters.cdbport)
    END IF 

    LET url = url, SFMT("/ws/r/percoProdServer/%1",
                     parameters.cdbgroup)

    LET url = url, SFMT("/ws/r/percoProdServer/%1/%2?userid=%3",
                     parameters.cdbgroup,
                     parameters.fcomando,
                     parameters.userid)

{
    
    IF parameters.cdbport = 80 THEN
       

    ELSE 
    
    END IF 
    LET url = SFMT("%1://%2:%3",
                   parameters.cdbprot,
                   parameters.cdbhost,
                   parameters.cdbport)

    IF parameters.cdbgas=="Y" THEN
    
       LET url = url, SFMT("%1/ws/r%2/dbsync_perco_server",
                           "/"||parameters.cdbconnector,
                           "/"||parameters.cdbgroup)
    END IF
    --LET url = url, "/ws/r/dbsync_perco_server/mobile/dbsync/", parameters.cdbformat
    LET url = url, "/ws/r/dbsync_perco_server/mobile/dbsync/", parameters.cdbformat}
    --DISPLAY "URL lleva -> ", url
    RETURN url
END FUNCTION

PUBLIC FUNCTION load_settings()
    {DEFINE fn, data STRING,
           ch base.Channel,
           ft BOOLEAN
    LET fn = get_settings_file()
    LET ch = base.Channel.create()
    TRY
        CALL ch.openFile(fn,"r")
        LET data = ch.readLine()
        CALL util.JSON.parse( data, parameters )
        CALL ch.close()
        LET ft = FALSE
    CATCH
        LET ft = TRUE
    END TRY
    IF ft THEN}

       LET parameters.userid = '333DDA5F-B8BC-4CE1-84EC-069F51C3490'
       LET parameters.cdbformat = "json"
       LET parameters.cdbprot = "http"
       LET parameters.cdbhost = "www.perfumeriafetiche.net" -- (for Android Emulator)
       LET parameters.cdbport = 6494 -- 6394 when using GAS
       LET parameters.cdbgas = "N"
       LET parameters.cdbconnector = 'ws/r' --NULL
       LET parameters.cdbgroup = 'productos'  --NULL
       LET parameters.fmetodo = 'GET'
       LET parameters.fcomando = 'existencia'
       LET parameters.fquery = 'SELECT barra, casa, correl, linea, tamano, medida, precio, existd FROM exi_online3'
       
       
       --LET parameters.auto_sync = 0
       --LET parameters.sync_geoloc = "Y"
       {IF NOT edit_settings(TRUE) THEN
          RETURN -1
       END IF
       CALL save_settings()
       LET parameters.see_all   = FALSE
       LET parameters.last_sync = "1900-01-01 00:00:00"}
       {RETURN 0
    ELSE
       RETURN 1
    END IF}
END FUNCTION