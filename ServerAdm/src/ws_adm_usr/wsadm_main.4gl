IMPORT security
DATABASE modula

CONSTANT cRaiz  = -1
CONSTANT cErr   = "Error. La operaci?n no fue procesada."
CONSTANT cAddOK = "Registro agregado exitosamente"
CONSTANT cDelOK = "Registro eliminado"
CONSTANT cUpdOK = "Registro actualizado"

MAIN
    CLOSE WINDOW screen
    CALL usuario()
END MAIN

FUNCTION usuario()

    DEFINE rec RECORD
        id LIKE ws_adm_user.id,
        username LIKE ws_adm_user.username,
        userid LIKE ws_adm_user.userid,
        userexpires LIKE ws_adm_user.userexpires
    END RECORD

    DEFINE qry STRING
    DEFINE resp SMALLINT

    OPEN WINDOW w1 WITH FORM "wsadm_form" ATTRIBUTES(TEXT = "Catalogo de Usuarios")

    MENU ""
        BEFORE MENU
            HIDE OPTION ALL
            SHOW OPTION "buscar", "agregar"
            GOTO busca
        ON ACTION buscar ATTRIBUTES(TEXT="Buscar")
            LABEL busca:
            SHOW OPTION ALL
            LET qry = "SELECT id, username, userid, userexpires FROM ws_adm_user"
            PREPARE prpCU1 FROM qry
            DECLARE curCU1 SCROLL CURSOR WITH HOLD FOR prpCU1  
            OPEN curCU1
            FETCH FIRST curCU1 INTO rec.id, rec.username, rec.userid, rec.userexpires
            IF STATUS = NOTFOUND THEN
                CALL msg("No existen datos en la tabla")
                HIDE OPTION ALL
                SHOW OPTION "buscar", "agregar"
            END IF
            DISPLAY BY NAME rec.*

        ON ACTION agregar ATTRIBUTES(TEXT="Agregar")
            LET int_flag = FALSE
            LET rec.id = 0
            INPUT BY NAME rec.*
               BEFORE INPUT 
                  LET rec.id = 0
                BEFORE FIELD userid
                    LET rec.userid =
                        security.RandomGenerator.CreateUUIDString()
                    DISPLAY BY NAME rec.userid
                    NEXT FIELD NEXT

                AFTER INPUT
                    IF (NOT int_flag) THEN
                        BEGIN WORK
                        INSERT INTO ws_adm_user(id, username, userid, userexpires)
                           VALUES(rec.id, rec.username, rec.userid, rec.userexpires)
                        IF STATUS = 0 THEN
                            COMMIT WORK
                            CALL msg(cAddOK)
                            CLOSE curCU1
                            OPEN curCU1
                            FETCH LAST curCU1 INTO rec.id, rec.username, rec.userid, rec.userexpires
                            DISPLAY BY NAME rec.*
                        ELSE
                            ROLLBACK WORK
                            CALL msg(cErr)
                        END IF
                    ELSE
                        LET int_flag = TRUE
                    END IF
            END INPUT
            --CLEAR FORM

        ON ACTION actualizar ATTRIBUTES(TEXT="Actualizar")
            IF rec.id <> cRaiz THEN
                LET int_flag = FALSE
                INPUT BY NAME rec.* WITHOUT DEFAULTS
                    AFTER INPUT
                        IF (NOT int_flag) THEN
                            BEGIN WORK
                            LET qry =
                                "UPDATE ws_adm_user ",
                                " SET ws_adm_user.username = ?, ws_adm_user.userid = ?, ws_adm_user.userexpires = ? ",
                                " WHERE ws_adm_user.id = ?"
                            PREPARE prpCUU FROM qry
                            EXECUTE prpCUU USING rec.username, rec.userid, rec.userexpires, rec.id
                            IF STATUS = 0 THEN
                                COMMIT WORK
                                CALL msg(cUpdOK)
                                CLOSE curCU1
                                OPEN curCU1
                                FETCH FIRST curCU1 INTO rec.id, rec.username, rec.userid, rec.userexpires
                                DISPLAY BY NAME rec.*
                            ELSE
                                ROLLBACK WORK
                                CALL msg(cErr)
                            END IF
                        ELSE
                            LET int_flag = TRUE
                        END IF
                END INPUT
                CLEAR FORM
                HIDE OPTION ALL
                SHOW OPTION "buscar", "agregar"
            END IF

        ON ACTION eliminar ATTRIBUTES(TEXT="Eliminar")
            IF rec.id <> cRaiz THEN
                LET resp = FALSE
                LET resp =
                    confirma("Esta seguro que desea eliminar el registro?")
                IF resp THEN
                    BEGIN WORK
                    LET qry =
                        "DELETE FROM ws_adm_user WHERE ws_adm_user.id = ", rec.id
                    PREPARE prpCUD FROM qry
                    EXECUTE prpCUD
                    IF STATUS = 0 THEN
                        COMMIT WORK
                        CALL msg(cDelOk)
                        CLOSE curCU1
                        OPEN curCU1
                        FETCH FIRST curCU1 INTO rec.id, rec.username, rec.userid, rec.userexpires
                        DISPLAY BY NAME rec.*
                    ELSE
                        CALL msg(cErr)
                        ROLLBACK WORK
                    END IF
                END IF
                CLEAR FORM
                HIDE OPTION ALL
                SHOW OPTION "buscar", "agregar"
            END IF

        ON ACTION primero ATTRIBUTES(TEXT="Primero")
            FETCH FIRST curCU1 INTO rec.id, rec.username, rec.userid, rec.userexpires
            DISPLAY BY NAME rec.*

        ON ACTION siguiente ATTRIBUTES(TEXT="Siguiente")
            FETCH NEXT curCU1 INTO rec.id, rec.username, rec.userid, rec.userexpires
            DISPLAY BY NAME rec.*

        ON ACTION anterior ATTRIBUTES(TEXT="Anterior")
            FETCH PREVIOUS curCU1 INTO rec.id, rec.username, rec.userid, rec.userexpires
            DISPLAY BY NAME rec.*

        ON ACTION ultimo ATTRIBUTES(TEXT="Ultimo")
            FETCH LAST curCU1 INTO rec.id, rec.username, rec.userid, rec.userexpires
            DISPLAY BY NAME rec.*

        ON ACTION salir ATTRIBUTES(TEXT="Salir")
            EXIT MENU
    END MENU
    CLOSE WINDOW w1

END FUNCTION

FUNCTION msg(msg)
    DEFINE msg STRING
    CALL box_valdato(msg)
END FUNCTION

FUNCTION confirma(msg)
    DEFINE msg STRING
    DEFINE res SMALLINT
    LET res = FALSE
    MENU "Confirmacion..."
        ATTRIBUTES(STYLE = "dialog", COMMENT = msg, IMAGE = "question")
        COMMAND "Si"
            LET res = TRUE
            EXIT MENU
        COMMAND "No"
            LET res = FALSE
            EXIT MENU
    END MENU
    RETURN res
END FUNCTION

FUNCTION box_valdato(mensaje)
    DEFINE mensaje CHAR(100)

    MENU "Advertencia"
        ATTRIBUTE(STYLE = "dialog",
            COMMENT = mensaje CLIPPED,
            IMAGE = "exclamation")
        COMMAND "Aceptar"
            LET int_flag = FALSE
            EXIT MENU
    END MENU
END FUNCTION

