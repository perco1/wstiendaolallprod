#+ exi(x,y) RETURING INTEGER
#+
#+ Consulta de existencia de un producto
#+
#+ @param x INTEGER
#+ 
#+ @param y INTEGER
#+
#+ @return INTEGER The result of subtracting the second parameter from the first
IMPORT util 
DATABASE modula
FUNCTION existencia(    
    userid STRING ATTRIBUTES(WSQuery)    --Parámetro 1 - Id de Usuario
    --CS y CHAR(3) ATTRIBUTES(WSQuery)   --Parámetro 2
    )
    ATTRIBUTES (WSGet, WSPath='/existencia') --Método que consume
    RETURNS STRING 
    
    DEFINE g_reg RECORD 
      --CS ldescription CHAR(15),        --Valor a retornar 1
      --CS lunit_price DECIMAL(8,2),     --Valor a retornar 2
      det DYNAMIC ARRAY OF RECORD   --Arreglo que retorna
         barra    CHAR(13),
         casa     CHAR(2),
         correl   INTEGER,
         linea    CHAR(3),
         tamano   CHAR(10),
         medida   CHAR(10),
         precio   DECIMAL(10,2),
         existd   DECIMAL(12,2) 
      END RECORD 
    END RECORD 
    
    DEFINE dataresp STRING          --String JSON a retornar
    DEFINE i INTEGER                --Contador 
    
    --Consulta encabezado
    
    SELECT id FROM ws_adm_user WHERE userid = @userid
    IF sqlca.sqlcode = 0 THEN
       --Consutal del detalle
       --CS DECLARE cur CURSOR FOR SELECT order_num, quantity FROM items WHERE stock_num =  x AND manu_code = y
       --DECLARE cur CURSOR FOR SELECT FIRST 10 casa, correl, preciov, existd FROM inv_exi WHERE existd > 0 ORDER BY 1,2 
       DECLARE cur CURSOR FOR SELECT barra, casa, correl, linea, tamano, medida, precio, existd FROM exi_online3 --ORDER BY 1,2 --WHERE existd > 0 ORDER BY 1,2 
       LET i = 1
       FOREACH cur INTO g_reg.det[i].barra, g_reg.det[i].casa, g_reg.det[i].correl, g_reg.det[i].linea, 
         g_reg.det[i].tamano, g_reg.det[i].medida, g_reg.det[i].precio, g_reg.det[i].existd
         LET i = i + 1
         --DISPLAY "i lleva ", i
       END FOREACH 
       CALL g_reg.det.deleteElement(i) 
    ELSE 
       LET g_reg.det[1].barra   = "ER"
       LET g_reg.det[1].casa    = "ER"
       LET g_reg.det[1].correl  = 0
       LET g_reg.det[1].linea   = "ERR"
       LET g_reg.det[1].tamano  = "ERROR EN C"
       LET g_reg.det[1].medida  = "ERROR EN C"
       LET g_reg.det[1].precio  = 0.0
       LET g_reg.det[1].existd  = 0.0  
    END IF
    
    LET dataresp = util.JSON.stringify (g_reg)  --Empacar la respuesta
    --DISPLAY "Respuesta =====> ", dataresp
    
   RETURN dataresp                              --Retornar la respuesta
END FUNCTION