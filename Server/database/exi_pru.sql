  drop view exi_online3;
create view exi_online3 (bodega,cod_barra,casa,correl,preciov,existd) as

--bodega central
select ie.bodega,max(r.cod_barra) ,ie.casa,ie.correl,ie.preciov,ie.existd-
case when(select sum(unidades) from invdet id inner join inv_tip it on id.tipo = it.tipo
                  where id.casa = ie.casa
                  and id.correl = ie.correl
                  and id.estatus = 'P'
                  and id.tipo = it.tipo
                  and it.efecto not in ('E','C')  )
                 is null then 0
           else
              (select sum(unidades) from invdet id inner join inv_tip it on id.tipo = it.tipo
                  where id.casa = ie.casa
                  and id.correl = ie.correl
                  and id.estatus = 'P'
                  and id.tipo = it.tipo
                  and it.efecto not in ('E','C')  )
           END
  from inv_exi ie  , inv_rel r
where ie.bodega = 1
and ie.correl between 600000 and 699999
and ie.existd > 4  -- se cambia
and r.casa = ie.casa
and r.corr = ie.correl


     and (ie.existd -
CASE WHEN(select sum(unidades) from invdet id inner join inv_tip it on id.tipo = it.tipo
                  where id.casa = ie.casa
                  and id.correl = ie.correl
                  and id.estatus = 'P'
                  and id.tipo = it.tipo
                  and it.efecto not in ('E','C')  )
                 is null then 0
           else
              (select sum(unidades) from invdet id inner join inv_tip it on id.tipo = it.tipo
                  where id.casa = ie.casa
                  and id.correl = ie.correl
                  and id.estatus = 'P'
                  and id.tipo = it.tipo
                  and it.efecto not in ('E','C')  )
           END) > 4

group by 1,3,4,5,6


union
select inv_exi.bodega,max(cod_barra), inv_exi.casa, inv_exi.correl,preciov,existd
from inv_exi  ,inv_rel
where bodega = 1
and correl not between 600000 and 699999
and existd > 4  -- se cambia
and inv_rel.casa = inv_exi.casa
and inv_rel.corr = inv_exi.correl

group by 1,3,4,5,6
{
--bodega central
select bodega, casa, correl,existd
from inv_exi
where bodega = 1
and existd > 4
}

union

--tienda avia (bodega=72)
select bodega,max(cod_barra), exist_av.casa, exist_av.correl,preciov,existd
from exist_av, inv_rel
where bodega = 72
and existd > 1
and inv_rel.casa = exist_av.casa
and inv_rel.corr = exist_av.correl
group by 1,3,4,5,6
union

--tienda reforma (bodega=2)
select bodega,max(cod_barra), exist_p.casa, exist_p.correl,preciov,existd
from exist_p, inv_rel
where bodega = 2
and existd > 1
and inv_rel.casa = exist_p.casa
and inv_rel.corr = exist_p.correl
group by 1,3,4,5,6
union

--tienda oakland (bodega=58)
select bodega,max(cod_barra), exist_o.casa, exist_o.correl,preciov,existd
from exist_o, inv_rel
where bodega = 58
and existd > 1
and inv_rel.casa = exist_o.casa
and inv_rel.corr = exist_o.correl
group by 1,3,4,5,6

union

--tienda cemaco (bodega=22)
select bodega,max(cod_barra), exist_c.casa, exist_c.correl,preciov,existd
from exist_c, inv_rel
where bodega = 22
and existd > 1
and inv_rel.casa = exist_c.casa
and inv_rel.corr = exist_c.correl
group by 1,3,4,5,6
