# wsTiendaOLAllProd

Versión 1.0
Desarrollada en 192.168.43.129
Genero 4.01
Fecha: 22/02/2024
Path: /app/Perco/Git/wstiendaolallprod
Tiene 3 aplicaciones:
1. Server - WebService
2. Client - Aplicación para comprobar el funcionamiento del WebService
3. Adm - Para crear los usuarios con userid=UUID

WSSrv prod  : http://192.168.1.12/gas/ws/r/percoProdServer/productos/ws/r/percoProdServer/productos/existencia?userid=333DDA5F-B8BC-4CE1-84EC-069F51C3490
Cliente prod: http://192.168.1.12/gas/ua/r/WSAllTest
Adm prod:     http://192.168.1.12/gas/ua/r/WSAllAdm

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!


